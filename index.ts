interface GroupCoins {
    [key: number]: number;
}

class Vault {

    private _coin: Coin[] = [];
    add(coin: Coin): void {
        this._coin.push(coin);
    }

    calcularTotal(): number {
        return this._coin.reduce((count, coin) => count + coin.value, 0);
    }

    getValorMenorMoeda(): number {
        const coin = this.getMenorMoeda();
        return coin ? coin.value : 0;
    }

    getMenorMoeda(): Coin | null {
        return this._coin.length ? this._coin.sort((a, b) => a.value - b.value)[0] : null;
    }

    getGruposMoedas(): GroupCoins {
        return this._coin.reduce((acumulador, moeda) => ({
            ...acumulador,
            [moeda.value]: (acumulador[moeda.value] || 0) + 1
        }), {} as GroupCoins)
    }

}

class Coin {

    constructor(private _value: number, private _name: string) { }

    get value() {
        return this._value;
    }

    get name() {
        return this._name;
    }

    toJSON(): string {
        return JSON.stringify({
            name: this._name,
            value: this._value,
        })
    }

}

const cofrinho = new Vault();

cofrinho.add(new Coin(5, '$'));
cofrinho.add(new Coin(10, '$'));
cofrinho.add(new Coin(3, '$'));
cofrinho.add(new Coin(5, '$'));
cofrinho.add(new Coin(10, '$'));
cofrinho.add(new Coin(5, '$'));

console.log('Moedas: ', cofrinho.getGruposMoedas());
console.log('=-=-=-=-=-=-=-=-=');
console.log('Valor menor moeda: ', cofrinho.getValorMenorMoeda());
console.log('=-=-=-=-=-=-=-=-=');
console.log('Menor moeda', cofrinho.getMenorMoeda());
console.log('=-=-=-=-=-=-=-=-=');
console.log('subtotal: ', cofrinho.calcularTotal());